FROM pull-through.cloud.duke.edu/hashicorp/vault:latest as vault

FROM pull-through.cloud.duke.edu/hashicorp/packer:latest as packer

FROM pull-through.cloud.duke.edu/minio/mc:latest as mc

FROM pull-through.cloud.duke.edu/library/golang as govc-getter
ENV GO111MODULE on
RUN git clone https://github.com/vmware/govmomi.git ${GOPATH}/src/github.com/vmware/govmomi && \
    cd ${GOPATH}/src/github.com/vmware/govmomi/govc && go install .

FROM pull-through.cloud.duke.edu/library/golang as mustache-getter
ENV GO111MODULE on
RUN go install github.com/cbroglie/mustache/cmd/mustache@v1.3.1

FROM pull-through.cloud.duke.edu/library/python:3-bookworm
COPY --from=vault bin/vault /usr/local/bin/
COPY --from=packer bin/packer /usr/local/bin/
COPY --from=govc-getter  /go/bin/govc /usr/local/bin/
COPY --from=mustache-getter /go/bin/mustache /usr/local/bin/
COPY --from=mc /usr/bin/mc /usr/local/bin
RUN vault version; packer version; mc -v; govc version;
RUN mkdir /.mc /.govmomi && \
    chgrp -R 0 /.mc /.govmomi && \
    chmod -R g=u /.mc /.govmomi
RUN apt-get update && \
    apt-get install -y \
        ansible \
        jq \
        pwgen \
        ansible \
        genisoimage \
        p7zip \
        xorriso \
        cloud-image-utils && \
    apt-get upgrade -y && \
    apt-get clean all
COPY ${WORKDIR}/main-builder/requirements.txt .
RUN pip install -r requirements.txt
RUN mkdir -p ~/.ssh; ssh-keyscan gitlab.oit.duke.edu > ~/.ssh/known_hosts
RUN git clone --depth=1 https://gitlab.oit.duke.edu/oit-ssi-systems/packer-shared-scripts /scripts
RUN useradd -m packer
USER packer
