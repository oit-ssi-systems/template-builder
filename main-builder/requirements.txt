-i https://pypi.org/simple
certifi==2019.11.28
chardet==3.0.4
idna==2.9
jinja2==2.11.1
markupsafe==1.1.1
requests==2.23.0
urllib3==1.25.8
pyyaml==5.3.1
